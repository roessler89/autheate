<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'master_password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'master_password', 'remember_token'
    ];

    /**
     * Get the Credentials related to the User
     */
    public function credentials() {
        return $this->hasMany('App\Credential');
    }

    /**
     * Get the Formulas related to the User
     */
    public function formulas() {
        return $this->hasMany('App\Formula');
    }
}
