<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credential extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password', 'url', 'note'
    ];

    /**
     * Get the User that owns the Credential.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the Formula that the Credential was created with.
     */
    public function formula() {
        return $this->belongsTo('App\Formula');
    }
}
