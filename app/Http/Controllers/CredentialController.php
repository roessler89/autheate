<?php

namespace App\Http\Controllers;

use App\Credential;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CredentialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $user = Auth::user();

            return $user->credentials->load('formula');
        }

        return redirect()->route('home');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'secret' => 'required',
            'password' => 'required'
        ]);

        $credential = new Credential;

        $credential->name = $request->input('name');
        $credential->username = $request->input('username');
        $credential->email = $request->input('email');
        $credential->secret = $request->input('encrypted_secret');
        $credential->password = $request->input('encrypted_password');
        $credential->url = $request->input('url');
        $credential->note = $request->input('note');
        $credential->user_id = Auth::user()->id;
        $credential->formula_id = $request->input('formula_id');

        if($credential->save()) {
            return response()->json([
                'saved' => true,
                'credential' => $credential->load('formula')
            ]);
        }

        return response()->json([
            'saved' => false
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Credential  $credential
     * @return \Illuminate\Http\Response
     */
    public function show(Credential $credential)
    {
        return $credential->load('formula');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Credential  $credential
     * @return \Illuminate\Http\Response
     */
    public function edit(Credential $credential)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Credential  $credential
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Credential $credential)
    {
        $this->validate($request, [
            'name' => 'required',
            'secret' => 'required',
            'password' => 'required'
        ]);

        $credential->name = $request->input('name');
        $credential->username = $request->input('username');
        $credential->email = $request->input('email');
        $credential->secret = $request->input('encrypted_secret');
        $credential->password = $request->input('encrypted_password');
        $credential->url = $request->input('url');
        $credential->note = $request->input('note');
        $credential->user_id = Auth::user()->id;
        $credential->formula_id = $request->input('formula_id');

        if($credential->save()) {
            return response()->json([
                'saved' => true,
                'credential' => $credential->load('formula')
            ]);
        }

        return response()->json([
            'saved' => false
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Credential  $credential
     * @return \Illuminate\Http\Response
     */
    public function destroy(Credential $credential)
    {
        if($credential->delete()) {
            return response()->json([
                'deleted' => true
            ]);
        }

        return response()->json([
            'deleted' => false
        ]);
    }
}
