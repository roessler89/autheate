<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Credential;

class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.dashboard');
    }

    /**
     * Return a listing of the credentials without personal information.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function credentials(Request $request)
    {
        if($request->ajax()) {
            return Credential::select('created_at')->get();
        }
    }

    /**
     * Display a listing of the users.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function users(Request $request)
    {
        if($request->ajax()) {
            return User::all();
        }

        return view('admin.users');
    }

    /**
     * Update the specified user in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updateUser(Request $request, User $user)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$request->input('id'),
            'role' => 'required|in:user,admin'
        ]);

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->role = $request->input('role');

        if($user->save()) {
            return response()->json([
                'saved' => true,
                'user' => $user
            ]);
        }

        return response()->json([
            'saved' => false
        ]);
    }
}
