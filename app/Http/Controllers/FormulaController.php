<?php

namespace App\Http\Controllers;

use App\Formula;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FormulaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            $user = Auth::user();

            return $user->formulas;
        }

        return view('formulas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'useVowelOffset' => 'required|boolean',
            'vowelOffset' => 'required|integer',
            'useConsonantOffset' => 'required|boolean',
            'consonantOffset' => 'required|integer',
            'countVowels' => 'required|boolean',
            'countConsonants' => 'required|boolean',
            'locateCapitals' => 'required|boolean',
            'useMajorSystem' => 'required|boolean',
            'default' => 'boolean'
        ]);

        $formula = new Formula;

        $formula->name = $request->input('name');
        $formula->useVowelOffset = $request->input('useVowelOffset');
        $formula->vowelOffset = $request->input('vowelOffset');
        $formula->useConsonantOffset = $request->input('useConsonantOffset');
        $formula->consonantOffset = $request->input('consonantOffset');
        $formula->countVowels = $request->input('countVowels');
        $formula->countConsonants = $request->input('countConsonants');
        $formula->locateCapitals = $request->input('locateCapitals');
        $formula->symbol = $request->input('symbol');
        $formula->useMajorSystem = $request->input('useMajorSystem');
        $formula->default = false;
        $formula->user_id = Auth::user()->id;

        if($formula->save()) {
            return response()->json([
                'saved' => true,
                'formula' => $formula
            ]);
        }

        return response()->json([
            'saved' => false
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    public function show(Formula $formula)
    {
        return $formula->credentials;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    public function edit(Formula $formula)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Formula $formula)
    {
        $this->validate($request, [
            'name' => 'required',
            'useVowelOffset' => 'required|boolean',
            'vowelOffset' => 'required|integer',
            'useConsonantOffset' => 'required|boolean',
            'consonantOffset' => 'required|integer',
            'countVowels' => 'required|boolean',
            'countConsonants' => 'required|boolean',
            'locateCapitals' => 'required|boolean',
            'useMajorSystem' => 'required|boolean',
            'default' => 'boolean'
        ]);

        $formula->name = $request->input('name');
        $formula->useVowelOffset = $request->input('useVowelOffset');
        $formula->vowelOffset = $request->input('vowelOffset');
        $formula->useConsonantOffset = $request->input('useConsonantOffset');
        $formula->consonantOffset = $request->input('consonantOffset');
        $formula->countVowels = $request->input('countVowels');
        $formula->countConsonants = $request->input('countConsonants');
        $formula->locateCapitals = $request->input('locateCapitals');
        $formula->symbol = $request->input('symbol');
        $formula->useMajorSystem = $request->input('useMajorSystem');
        $formula->default = $request->input('default');
        $formula->user_id = Auth::user()->id;

        if($formula->default) {
            Formula::where('default', 1)->update(['default' => 0]);
        }

        if($formula->save()) {
            return response()->json([
                'saved' => true,
                'formula' => $formula
            ]);
        }

        return response()->json([
            'saved' => false
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Formula  $formula
     * @return \Illuminate\Http\Response
     */
    public function destroy(Formula $formula)
    {
        if($formula->delete()) {
            return response()->json([
                'deleted' => true
            ]);
        }

        return response()->json([
            'deleted' => false
        ]);
    }
}
