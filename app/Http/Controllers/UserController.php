<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['recaptcha']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {
            return User::all();
        }

        return User::all();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function show(User $user, Request $request)
    {
        if($request->ajax()) {
            return $user;
        }

        return $user;
    }

    /**
     * Return the current user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function current(Request $request)
    {
        if($request->ajax()) {
            $user = Auth::user();

            return $user;
        }
    }

    /**
     * Show the view for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function account()
    {
        return view('my-account');
    }

    /**
     * Return a boolean based on password verification (check entered password is user's password).
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {
        if($request->ajax()) {
            if(Hash::check($request->input('password'), Auth::user()->password)) {
                return response()->json([
                    'verified' => true
                ]);
            } else {
                return response(['password' => ['The password you entered does not match what we have in our system.']], 422);
            }
        }
    }

    /**
     * Return a boolean based on Google's Invisible reCAPTCHA verification. This action is used as a proxy to prevent
     * XSS attacks and any "Access-Control" errors.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function recaptcha(Request $request)
    {
        if($request->ajax()) {
            header('Content-type: application/json');

            $response = $request->input('response');
            $secret = $request->input('secret');

            $json = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$response);

            return $json;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $currentPassword = $request->input('currentPassword');
        $password = $request->input('password');

        if(isset($currentPassword) && isset($password)) {
            $validateArray = [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users,email,'.$request->input('id'),
                'password' => 'required|min:6|confirmed'
            ];
        } else {
            $validateArray = [
                'name' => 'required|max:255',
                'email' => 'required|email|max:255|unique:users,email,'.$request->input('id')
            ];
        }

        $this->validate($request, $validateArray);

        $user->name = $request->input('name');
        $user->email = $request->input('email');

        if(isset($currentPassword) && isset($password)) {
            if(Hash::check($request->input('currentPassword'), Auth::user()->password)) {
                $user->password = Hash::make($request->input('password'));
            } else {
                return response(['currentPassword' => ['The current password you entered does not match what we have in our system.']], 422);
            }
        }

        if($user->save()) {
            return response()->json([
                'saved' => true,
                'user' => $user
            ]);
        }

        return response()->json([
            'saved' => false
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
