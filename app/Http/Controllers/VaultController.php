<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class VaultController extends Controller
{
    /**
     * Return the master password used for encryption.
     *
     * @return master_password or false
     */
    public function __invoke()
    {
        $this->middleware('auth');

        $user = Auth::user();

        if(isset($user->master_password)) {
            return $user->master_password;
        }

        return 0;
    }
}
