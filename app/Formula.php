<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'useVowelOffset', 'vowelOffset', 'useConsonantOffset', 'consonantOffset', 'countVowels',
        'countConsonants', 'locateCapitals', 'symbol', 'useMajorSystem', 'default'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'useVowelOffset' => 'boolean',
        'vowelOffset' => 'integer',
        'useConsonantOffset' => 'boolean',
        'consonantOffset' => 'integer',
        'countVowels' => 'boolean',
        'countConsonants' => 'boolean',
        'locateCapitals' => 'boolean',
        'useMajorSystem' => 'boolean',
        'default' => 'boolean'
    ];

    /**
     * Get the User that owns the Formula.
     */
    public function user() {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the Credentials that were created with the Formula.
     */
    public function credentials() {
        return $this->hasMany('App\Credential');
    }
}
