<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormulasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('formulas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('name');
            $table->boolean('useVowelOffset');
            $table->integer('vowelOffset');
            $table->boolean('useConsonantOffset');
            $table->integer('consonantOffset');
            $table->boolean('countVowels');
            $table->boolean('countConsonants');
            $table->boolean('locateCapitals');
            $table->string('symbol')->nullable();
            $table->boolean('useMajorSystem');
            $table->boolean('default')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('formulas');
    }
}
