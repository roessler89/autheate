<?php

use Illuminate\Database\Seeder;

class FormulasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('formulas')->insert([
            'name' => 'Autheate Standard',
            'useVowelOffset' => true,
            'vowelOffset' => -3,
            'useConsonantOffset' => true,
            'consonantOffset' => 3,
            'countVowels' => true,
            'countConsonants' => true,
            'locateCapitals' => true,
            'symbol' => '!',
            'useMajorSystem' => false,
            'default' => true,
            'user_id' => 1
        ]);
    }
}
