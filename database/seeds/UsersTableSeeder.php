<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Jeffrey Roessler',
            'email' => 'roessler@rhits.com.au',
            'password' => bcrypt('peter46'),
            'master_password' => bcrypt(md5('peter46'.random_bytes(32))),
            'created_at' => Carbon\Carbon::now(),
            'updated_at' => Carbon\Carbon::now(),
            'role' => 'admin'
        ]);
    }
}
