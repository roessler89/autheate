## About Autheate

Autheate is a password manager designed to help you remember your passwords, not just store them. With Autheate you can create your own personalised formulas, which will help you to create strong passwords that you can remember.

## Learning Autheate

Please view the FAQ section at https://autheate.com/faq.

## Contributing

Thank you for considering contributing to Autheate! As I am new to open source development, I don't have any guidelines set up. If you wish to contribute, please feel free to drop me a line jeffroessler89@gmail.com or create a pull request.

## Security Vulnerabilities

If you discover a security vulnerability within Autheate, please send an e-mail to Jeffrey Roessler at jeffroessler89@gmail.com. All help is greatly appreciated and helps me to become a better developer.

## License

Autheate is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
