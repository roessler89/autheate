<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
</head>
<body>
    <div id="app">
        <!-- Loading Bar Section -->
        @include('layouts.components.loading-bar')
        <!-- End Loading Bar Section -->

        <!-- Nav Section -->
        @include('layouts.components.nav')
        <!-- End Nav Section -->

        <!-- Content Section -->
        <div v-if="!vaultLockedStatus || !user.id">
            @yield('content')
        </div>
        <!-- End Content Section -->

        <!-- Lock Timer Section -->
        <div v-cloak v-if="lockTimerViewable">
            @include('layouts.components.locktimer');
        </div>
        <!-- End Lock Timer Section -->

        <!-- Locked Section -->
        <div v-cloak v-if="vaultLockedStatus && user.id && loadDelay">
            @include('layouts.components.locked')
        </div>
        <!-- End Locked Section -->

        <!-- Footer Section -->
        <footer class="footer">
            <div class="container">
                <div class="content has-text-centered">
                    <p>
                        <strong>Autheate</strong>&copy; 2017 by <a href="http://jeffreyroessler.com">Jeffrey Roessler</a>.
                    </p>

                    <p><a class="icon" href="https://bitbucket.org/roessler89">
                            <i class="fa fa-bitbucket"></i>
                        </a>
                    </p>
                </div>
            </div>
        </footer>
        <!-- End Footer Section -->

        <!-- Notification Alerts -->
        <notification-alerts></notification-alerts>
        <!-- End Notification Alerts -->
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>
