<div class="modal is-active">
    <div class="modal-background"></div>

    <div class="modal-content">
        <div class="box">
            <div class="message">
                <div class="message-header">
                    <p>Your Vault will automatically lock in @{{ timeRemaining }}...</p>
                </div>

                <div class="message-body">
                    <p>To protect your information your vault is automatically locked after a period of inactivity of five (5) minutes on Autheate.</p>

                    <br/>

                    <p>If you navigate away from Autheate, close the tab or close the browser you will be logged out of your account to prevent unauthorised access to your information through your computer.</p>

                    <br/>

                    <p>To prevent your Vault from being locked, move your mouse or tap anywhere on the page.</p>
                </div>
            </div>
        </div>
    </div>
</div>
