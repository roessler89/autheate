<div class="columns">
    <div class="column">
        <h2 class="title is-2">
            Users
        </h2>
    </div>
</div>

<div class="columns">
    <div class="column">
        <h4 class="subtitle is-4">
            Click <a href="/admin/users">here</a> to view all.
        </h4>

        <admin-users limit="5"></admin-users>
    </div>
</div>