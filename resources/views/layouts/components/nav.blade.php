<nav class="nav has-shadow">
    <div class="container">
        <div class="nav-left">
            <a href="{{ url('/') }}" class="nav-item">
                <img src="/images/AutheateLogo.png" alt="Autheate Logo" style="margin-right: 5px;">
                <span>{{ config('app.name', 'Autheate') }}</span>
            </a>

            @if(!Auth::guest())
                <a href="{{ route('home') }}" class="nav-item is-tab <?= $_SERVER['REQUEST_URI'] == '/home' ? 'is-active' : '' ?>">
                    Home
                </a>
            @endif

            <a href="{{ route('about') }}" class="nav-item is-tab <?= $_SERVER['REQUEST_URI'] == '/about' ? 'is-active' : '' ?>">
                About
            </a>

            <a href="{{ route('about-formulas') }}" class="nav-item is-tab <?= $_SERVER['REQUEST_URI'] == '/about-formulas' ? 'is-active' : '' ?>">
                Learn Formulas
            </a>
        </div>

        <div class="nav-center">
            <a href="https://bitbucket.org/roessler89/autheate" class="nav-item">
                <span class="icon">
                    <i class="fa fa-bitbucket"></i>
                </span>
            </a>
        </div>

        <span class="nav-toggle">
        <span></span>
        <span></span>
        <span></span>
    </span>

        <div class="nav-right nav-menu">
            @if(Auth::guest())
                <span class="nav-item">
                    <a href="{{ route('register') }}" class="button is-warning">
                        <span class="icon">
                            <i class="fa fa-user-plus"></i>
                        </span>
                        <span>Sign Up</span>
                    </a>
                </span>

                <span class="nav-item">
                    <a href="{{ route('login') }}" class="button is-outlined is-primary">
                        <span class="icon">
                            <i class="fa fa-sign-in"></i>
                        </span>
                        <span>Login</span>
                    </a>
                </span>
            @else
                <a href="{{ route('home') }}" class="nav-item is-tab is-hidden-tablet <?= $_SERVER['REQUEST_URI'] == '/home' ? 'is-active' : '' ?>">
                    Home
                </a>

                <nav-user-dropdown></nav-user-dropdown>
            @endif
        </div>
    </div>
</nav>
