<div class="columns">
    <div class="column">
        <h2 class="title is-2">Autheate at a glance...</h2>
    </div>
</div>

<div class="columns">
    <div class="column">
        <h4 class="subtitle is-4">
            User Registrations
        </h4>

        <chart-user-registrations></chart-user-registrations>
    </div>

    <div class="column">
        <h4 class="subtitle is-4">
            Active Users
        </h4>

        <chart-active-users></chart-active-users>
    </div>

    <div class="column">
        <h4 class="subtitle is-4">
            Credentials
        </h4>

        <chart-credentials-created></chart-credentials-created>
    </div>
</div>