@extends('layouts.app')

@section('content')
    <section class="hero is-info">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">Formulas</h1>
                <h2 class="subtitle">Your formulas are listed below.</h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-2">
                    @include('layouts.components.menu')
                </div>

                <div class="column is-8">
                    @include('layouts.components.formulas')
                </div>

                <div class="column is-2">
                    @include('layouts.components.add-new-formula')
                </div>
            </div>
        </div>
    </section>
@endsection
