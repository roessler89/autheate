@extends('layouts.app')

@section('content')
    <section class="hero is-info">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">My Account</h1>
                <h2 class="subtitle">Make changes to your account settings and profile.</h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-2">
                    @include('layouts.components.menu')
                </div>

                <div class="column is-10">
                    @include('layouts.components.user-account')
                </div>
            </div>
        </div>
    </section>
@endsection
