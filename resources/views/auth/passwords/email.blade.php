@extends('layouts.app')

@section('content')
    <section class="hero is-info">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">
                    Reset Your Password
                </h1>
                <h2 class="subtitle">
                    To start the process, enter your email address and click "Send Password Reset Link".
                </h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-6 is-offset-3">
                    <div class="box">
                        <send-password-reset-form></send-password-reset-form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
