@extends('layouts.app')

@section('content')
    <section class="hero is-info">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">
                    Reset Your Password
                </h1>
                <h2 class="subtitle">
                    Let's reset your password. Enter your email address, new password, confirm it and click "Reset Password".
                </h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-6 is-offset-3">
                    <div class="box">
                        <input id="token" type="hidden" name="token" value="{{ $token }}">
                        <password-reset-form></password-reset-form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
