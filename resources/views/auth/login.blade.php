@extends('layouts.app')

@section('content')
    <section class="hero is-info">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">
                    Login
                </h1>
                <h2 class="subtitle">
                    And get Autheated.
                </h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-6 is-offset-3">
                    <div class="box">
                        <login-form></login-form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
