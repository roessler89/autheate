@extends('layouts.app')

@section('content')
    <section class="hero is-warning">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">Users</h1>
                <h2 class="subtitle">You are an administrator, please act accordingly.</h2>
            </div>
        </div>
    </section>

    <section class="section">
        <div class="container">
            <div class="columns">
                <div class="column is-2">
                    @include('layouts.components.menu')
                </div>

                <div class="column">
                    @include('layouts.components.admin-users')
                </div>
            </div>
        </div>
    </section>
@endsection
