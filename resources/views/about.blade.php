@extends('layouts.app')

@section('content')
    <section class="hero is-medium is-dark">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">About Autheate</h1>
                <h2 class="subtitle">Who, what, when, how, why?</h2>
            </div>
        </div>
    </section>

    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">Who?</h1>
                <h2 class="subtitle">Who is behind Autheate?</h2>
                <p>This is the brainchild of <a href="jeffreyroessler.com">Jeffrey Roessler</a>.</p>
            </div>
        </div>
    </section>

    <section class="hero is-dark">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">What?</h1>
                <h2 class="subtitle">What is Autheate?</h2>
                <p>Autheate is a password manager that utilises a combination of a secret word and a formula for each credential, helping you to remember every password you create (or Autheate ;) ).</p>
            </div>
        </div>
    </section>

    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">When?</h1>
                <h2 class="subtitle">When did Autheate launch?</h2>
                <p>Autheate officially launched in September of 2017.</p>
            </div>
        </div>
    </section>

    <section class="hero is-dark">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">How?</h1>
                <h2 class="subtitle">How does Autheate work?</h2>
                <p>Autheate was built using Laravel 5.4 for the backend, Vuejs for the front-end, Bulma.io for the CSS stylings.</p>
            </div>
        </div>
    </section>

    <section class="hero is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">Why?</h1>
                <h2 class="subtitle">Why does this exist?</h2>
                <p>I got fed up some time ago when trying to remember passwords I used on a daily basis, and having to use a password manager all the time does decrease productivity when you're constantly flicking back and forth from the login screen to the password manager. To combat this, I came up with some simple universal formulas that I apply to secret words specifically derived from the application or website I'm storing the credential for.</p>
                <br/>
                <p>This worked great, still does! But I'm a lazy person, so I decided to build an application that would do the grunt work for me, and store the credential as a general password manager. The reason I built a new app is because the other password managers do not allow the flexibility of creating formula based credentials.</p>
                <br/>
                <p>Another reason I decided to build this was to add to my public portfolio so that others can inspect the quality of my code.</p>
            </div>
        </div>
    </section>

    <section class="hero is-dark">
        <div class="hero-body">
            <div class="container has-text-centered">
                <a href="{{ route('about-formulas') }}" class="button is-primary">
                        <span>Learn about Formulas.</span>
                </a>

                <br/>
                <br/>

                <a href="{{ route('register') }}" class="button is-large is-warning">
                        <span class="icon">
                            <i class="fa fa-user-plus"></i>
                        </span>
                        <span>Create an account and get Autheated!</span>
                </a>
            </div>
        </div>
    </section>
@endsection
