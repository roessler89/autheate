@extends('layouts.app')

@section('content')
    <section class="hero is-medium is-dark">
        <div class="hero-body">
            <div class="container has-text-centered">
                <h1 class="title">Autheate</h1>
                <h2 class="subtitle">Don't just manage your passwords, remember them!</h2>
                <a href="{{ route('register') }}" class="button is-large is-dark">
                        <span class="icon">
                            <i class="fa fa-user-plus"></i>
                        </span>
                        <span>Get Started!</span>
                </a>
            </div>
        </div>
    </section>

    <section class="hero is-large is-info">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">Do you remember your passwords?</h1>
                <h2 class="subtitle">If you answered yes to this, do you use that password more than once? More than twice? For every account you own?</h2>
                <p>It's common these days for people across the world to implement one of two strategies when it comes to password or account management. Either they use the same password multiple times, to make it easier for them to remember it later. Or they utilise a password manager such as 1Password or LastPass, to create, store and manage the password for them - generally something like <code>QGR1RMl"1PdX</code>. Try remembering that!</p>
                <br/>
                <p>Whilst using a password manager is a great idea and something I encourage, the passwords they generate are often so complex that they are impossible to remember. What's ironic is that those passwords, stored safely in the vault of those applications, are accessed through a password that only the user can remember. What this means is that the password set by the user is generally something simple and easy to remember, which is also something easy to crack.</p>
                <br/>
                <p>That's a big problem. I don't need to crack all your account passwords across all the different websites you visit, no I simply attack the weakest link - that being the easy password you've set on your password manager.</p>
                <br/>
                <p>"OK, I'll just set a complex password then!" - If you thought this, excellent! Unfortunately, these complex passwords (as mentioned above) can be difficult to remember. So what happens when you forget your password and can no longer access any of the credentials you have in your password manager? Depending on which password manager you use, it can be a pain to reset your "Master Password", in some cases you may be permanently locked out of your password manager, losing all the credentials you need for your day-to-day life.</p>
                <br/>
                <p>Well, that's what Autheate is for. Autheate not only manages your credentials for you, but teaches you to create passwords with an easy to remember formula, so that if you are ever left without access to your password manager you can work out your password mentally.</p>
            </div>
        </div>
    </section>

    <section class="hero is-large is-dark">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">How does it work?</h1>
                <h2 class="subtitle">It's simple, let me show you.</h2>

                <div class="columns">
                    <div class="column">
                        <h3 class="is-3">1. Enter in some general information</h3>
                    </div>
                    <div class="column">
                        <img src="/images/step1.png" alt="Step 1: Enter general information." style="max-width: 400px;">
                    </div>
                </div>

                <div class="columns">
                    <div class="column">
                        <h3 class="is-3">2. Select the formula you want to use (or create your own)</h3>
                    </div>
                    <div class="column">
                        <img src="/images/step2.png" alt="Step 2: Select a formula or create your own." style="max-width: 400px;">
                    </div>
                </div>

                <div class="columns">
                    <div class="column">
                        <h3 class="is-3">3. Enter an easy to remember secret word</h3>
                    </div>
                    <div class="column">
                        <img src="/images/step3.png" alt="Step 1: Enter an easy to remember secret word." style="max-width: 400px;">
                    </div>
                </div>

                <div class="columns">
                    <div class="column">
                        <h3 class="is-3">4. Done! Your password has been Autheated!</h3>
                    </div>
                    <div class="column">
                        <img src="/images/finished.png" alt="Done! Your password has been Autheated!" style="max-width: 400px;">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="hero is-medium is-primary">
        <div class="hero-body">
            <div class="container has-text-centered">
                <a href="{{ route('register') }}" class="button is-large is-dark">
                        <span class="icon">
                            <i class="fa fa-user-plus"></i>
                        </span>
                        <span>Create an account and get Autheated!</span>
                </a>
            </div>
        </div>
    </section>
@endsection
