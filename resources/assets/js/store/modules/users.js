import users from '../../api/users';
import * as types from '../mutation-types';

const state = {
    users: [],
    user: {}
};

const getters = {
    users: state => state.users,
    user: state => state.user
};

const actions = {
    getUsers({commit}) {
        users.getUsers((users) => commit(types.GET_ALL_USERS, users));
    },

    getUser({ commit }) {
        users.getUser((user) => commit(types.GET_CURRENT_USER, user));
    },

    updateUser({ commit }, user) {
        commit(types.UPDATE_USER, user);
    }
};

const mutations = {
    [types.GET_ALL_USERS] (state, users) {
        state.users = users;
    },

    [types.GET_CURRENT_USER] (state, user) {
        state.user = user;
    },

    [types.UPDATE_USER] (state, user) {
        state.user = user;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}