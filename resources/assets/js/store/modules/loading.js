import * as types from '../mutation-types';

const state = {
    startedLoading: false,
    finishedLoading: false,
    loading: false,
    errorLoading: false
};

const getters = {
    startedLoading: state => state.startedLoading,
    finishedLoading: state => state.finishedLoading,
    loading: state => state.loading,
    errorLoading: state => state.errorLoading
};

const actions = {
    startLoading({ commit }) {
        commit(types.START_LOADING);
    },

    finishLoading({ commit }) {
        commit(types.FINISH_LOADING);
    },

    errorLoading({ commit }) {
        commit(types.ERROR_LOADING);
    }
};

const mutations = {
    [types.START_LOADING] (state) {
        state.startedLoading = true;
        state.finishedLoading = false;
        state.errorLoading = false;
        state.loading = true;
    },

    [types.FINISH_LOADING] (state) {
        state.finishedLoading = true;
        state.startedLoading = false;
        state.errorLoading = false;
        state.loading = false;
    },

    [types.ERROR_LOADING] (state) {
        state.errorLoading = true;
        state.startedLoading = false;
        state.finishedLoading = false;
        state.loading = false;
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}