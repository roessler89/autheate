import bank from '../../api/bank';
import * as types from '../mutation-types';

const state = {
    credentials: [],
    locked: true,
    master_password: false,
    loadingVault: true
};

const getters = {
    credentials: state => state.credentials,
    locked: state => state.locked,
    master_password: state => state.master_password,
    loadingVault: state => state.loadingVault
};

const actions = {
    unlockVault({ commit }) {
        bank.unlockVault(
            (master_password) => commit(types.UNLOCK_VAULT, master_password),
            (credentials) => commit(types.LOAD_CREDENTIALS, credentials),
            (status) => commit(types.LOADING_VAULT, status)
        );
    },

    lockVault({ commit }) {
        commit(types.LOCK_VAULT);
    },

    loadCredentials({ commit }) {
        bank.loadCredentials((credentials) => commit(types.LOAD_CREDENTIALS, credentials));
    },

    addNewCredential({ commit }, credential) {
        commit(types.ADD_TO_VAULT, credential);
    },

    updateCredential({ commit }, credential) {
        commit(types.UPDATE_CREDENTIAL, credential);
    },

    removeCredential({ commit }, credential) {
        commit(types.REMOVE_FROM_VAULT, credential);
    }
};

const mutations = {
    [types.LOADING_VAULT] (state, status) {
        state.loadingVault = status;
    },

    [types.UNLOCK_VAULT] (state, master_password) {
        state.master_password = master_password;
        state.locked = false;
    },

    [types.LOAD_CREDENTIALS] (state, credentials) {
        state.credentials = credentials;
    },

    [types.LOCK_VAULT] (state) {
        state.credentials = [];
        state.master_password = null;
        state.locked = true;
    },

    [types.ADD_TO_VAULT] (state, credential) {
        state.credentials.push(credential);
    },

    [types.UPDATE_CREDENTIAL] (state, credential) {
        let oldCredentialIndex = state.credentials.map((e) => { return e.id }).indexOf(credential.id);

        state.credentials[oldCredentialIndex] = credential;
    },

    [types.REMOVE_FROM_VAULT] (state, credential) {
        state.credentials = state.credentials.filter((item) => {
            return item !== credential;
        });
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}