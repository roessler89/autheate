import * as types from '../mutation-types';

const state = {
    notifications: []
};

const getters = {
    notifications: state => state.notifications
};

const actions = {
    addNewNotification({ commit }, notification) {
        commit(types.ADD_NOTIFICATION, notification);
    },

    removeNotification({ commit }, notification) {
        commit(types.REMOVE_NOTIFICATION, notification);
    }
};

const mutations = {
    [types.ADD_NOTIFICATION] (state, notification) {
        state.notifications.push(notification);
    },

    [types.REMOVE_NOTIFICATION] (state, notification) {
        state.notifications = state.notifications.filter((item) => {
            return item !== notification;
        });
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}