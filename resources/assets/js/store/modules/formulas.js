import formulas from '../../api/formulas';
import * as types from '../mutation-types';

const state = {
    formulas: [],
    loadingFormulas: true
};

const getters = {
    formulas: state => state.formulas,
    loadingFormulas: state => state.loadingFormulas
};

const actions = {
    getFormulas({commit}) {
        formulas.getFormulas(
            (formulas) => commit(types.GET_ALL_FORMULAS, formulas),
            (status) => commit(types.LOADING_FORMULAS, status)
        );
    },

    loadFormulas({commit}) {
        formulas.loadFormulas((formulas) => commit(types.GET_ALL_FORMULAS, formulas));
    },

    addNewFormula({ commit }, formula) {
        commit(types.ADD_FORMULA, formula);
    },

    updateFormula({ commit }, formula) {
        commit(types.UPDATE_FORMULA, formula);
    },

    removeFormula({ commit }, formula) {
        commit(types.REMOVE_FORMULA, formula);
    }
};

const mutations = {
    [types.LOADING_FORMULAS] (state, status) {
        state.loadingFormulas = status;
    },

    [types.GET_ALL_FORMULAS] (state, formulas) {
        state.formulas = formulas;
    },

    [types.ADD_FORMULA] (state, formula) {
        state.formulas.push(formula);
    },

    [types.UPDATE_FORMULA] (state, formula) {
        let oldFormulaIndex = state.formulas.map((e) => { return e.id }).indexOf(formula.id);

        state.formulas[oldFormulaIndex] = formula;
    },

    [types.REMOVE_FORMULA] (state, formula) {
        state.formulas = state.formulas.filter((item) => {
            return item !== formula;
        });
    }
};

export default {
    state,
    getters,
    actions,
    mutations
}