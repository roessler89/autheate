// User Mutation Types
export const GET_ALL_USERS = 'GET_ALL_USERS';
export const GET_CURRENT_USER = 'GET_CURRENT_USER';
export const UPDATE_USER = 'UPDATE_USER';
export const REMOVE_USER = 'REMOVE_USER';

// Credential Vault Mutation Types
export const LOADING_VAULT = 'LOADING_VAULT';
export const UNLOCK_VAULT = 'UNLOCK_VAULT';
export const LOCK_VAULT = 'LOCK_VAULT';
export const LOAD_CREDENTIALS = 'LOAD_CREDENTIALS';
export const GET_ALL_CREDENTIALS = 'GET_ALL_CREDENTIALS';
export const ADD_TO_VAULT = 'ADD_TO_VAULT';
export const REMOVE_FROM_VAULT = 'REMOVE_FROM_VAULT';
export const UPDATE_CREDENTIAL = 'UPDATE_CREDENTIAL';

// Formula Mutation Types
export const LOADING_FORMULAS = 'LOADING_FORMULAS';
export const GET_ALL_FORMULAS = 'GET_ALL_FORMULAS';
export const ADD_FORMULA = 'ADD_FORMULA';
export const REMOVE_FORMULA = 'REMOVE_FORMULA';
export const UPDATE_FORMULA = 'UPDATE_FORMULA';
export const SET_DEFAULT = 'SET_DEFAULT';

// Notification Mutation Types
export const ADD_NOTIFICATION = 'ADD_NOTIFICATION';
export const REMOVE_NOTIFICATION = 'REMOVE_NOTIFICATION';

// Loading Bar Mutation Types
export const START_LOADING = 'START_LOADING';
export const FINISH_LOADING = 'FINISH_LOADING';
export const ERROR_LOADING = 'ERROR_LOADING';