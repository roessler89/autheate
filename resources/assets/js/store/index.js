import Vue from 'vue';
import Vuex from 'vuex';
import * as actions from './actions';
import * as getters from './getters';
import users from './modules/users';
import vault from './modules/vault';
import formulas from './modules/formulas';
import notifications from './modules/notifications';
import loading from './modules/loading';

Vue.use(Vuex);

export default new Vuex.Store({
    actions,
    getters,
    modules: {
        users,
        vault,
        formulas,
        notifications,
        loading
    }
});