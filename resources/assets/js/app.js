/**
 * Load necessary project files and window assignments
 */
require('./bootstrap');

/**
 * Load Google Analytics
 */
require('./googleanalytics');

/**
 * Load Google Embed API
 */
require('./googleembedapi');

/**
 * Load Google Embed API Dependencies
 */
require('./active-users');
require('./date-range-selector');
require('./view-selector2');

/**
 * Import statements
 */
import Vuex from 'vuex';
import { mapGetters } from 'vuex';
import store from './store';
import VueProgressBar from 'vue-progressbar';
import { Form } from './classes/Form';

/**
 * Configure the Vue Progress Bar
 */
Vue.use(VueProgressBar, {
    color: 'rgb(143, 255, 199)',
    failedColor: 'red',
    height: '2px'
});

/**
 * Tell Vue to use Vuex
 */
Vue.use(Vuex);

/**
 * Import Vue Components
 */
// Admin Components
Vue.component('admin-users', require('./components/admin/Users.vue'));
Vue.component('admin-user', require('./components/admin/User.vue'));
Vue.component('chart-user-registrations', require('./components/admin/UserRegistrationsChart.vue'));
Vue.component('chart-credentials-created', require('./components/admin/CredentialsChart.vue'));
Vue.component('chart-active-users', require('./components/admin/ActiveUsersChart.vue'));

// Auth Components
Vue.component('register-form', require('./components/RegisterForm.vue'));
Vue.component('login-form', require('./components/LoginForm.vue'));
Vue.component('send-password-reset-form', require('./components/SendPasswordResetForm.vue'));
Vue.component('password-reset-form', require('./components/PasswordResetForm.vue'));

// Credential Components
Vue.component('credentials-list', require('./components/Credentials.vue'));
Vue.component('credential', require('./components/Credential.vue'));
Vue.component('new-credential', require('./components/NewCredential.vue'));
Vue.component('edit-credential', require('./components/EditCredential.vue'));

// Formula Components
Vue.component('formulas-list', require('./components/Formulas.vue'));
Vue.component('formula', require('./components/Formula.vue'));
Vue.component('new-formula-view', require('./components/NewFormulaView.vue'));
Vue.component('new-formula', require('./components/NewFormula.vue'));
Vue.component('edit-formula', require('./components/EditFormula.vue'));

// Layout Components
Vue.component('nav-user-dropdown', require('./components/NavUserDropdown.vue'));
Vue.component('app-menu', require('./components/AppMenu.vue'));

// Miscellaneous Components
Vue.component('confirm-action', require('./components/ConfirmAction.vue'));
Vue.component('flatpickr', require('./components/Flatpickr.vue'));
Vue.component('chartjs', require('./components/Chart.vue'));
Vue.component('locked', require('./components/Locked.vue'));
Vue.component('grecaptcha', require('./components/Recaptcha.vue'));

// Notification Components
Vue.component('notification-alerts', require('./components/NotificationAlerts.vue'));
Vue.component('notification-alert', require('./components/NotificationAlert.vue'));

// User Account Components
Vue.component('user-account', require('./components/UserAccount.vue'));

/**
 * Initial Vue Instance
 * @type {Vue}
 */

const app = new Vue({
    el: '#app',

    data: {
        maxIdleTime: 300000,
        timeOut: setTimeout(this.lockVault, this.maxIdleTime),
        timeOutStarted: (new Date()).getTime(),
        timeRemaining: moment(this.timeOutStarted + this.maxIdleTime).countdown().toString(),
        loadDelay: false
    },

    store,

    computed: {
        ...mapGetters({
            master_password: 'master_password',
            credentials: 'credentials',
            vaultLockedStatus: 'locked',
            startedLoading: 'startedLoading',
            finishedLoading: 'finishedLoading',
            errorLoading: 'errorLoading',
            user: 'user',
        }),

        displayLockTimer() {
            if(parseInt(this.timeRemaining.substr(0, this.timeRemaining.indexOf(' ')))) {
                if(parseInt(this.timeRemaining.substr(0, this.timeRemaining.indexOf(' '))) < 50 &&
                    this.timeRemaining.indexOf("minute") < 0) return true;
            }

            return false;
        },

        lockTimerViewable() {
            if(this.loadDelay && this.displayLockTimer && this.user.id && !this.vaultLockedStatus) return true;

            return false;
        }
    },

    watch: {
        startedLoading() {
            if(this.startedLoading) {
                this.$Progress.start();
            }
        },

        finishedLoading() {
            if(this.finishedLoading) {
                this.$Progress.finish();
            }
        },

        errorLoading() {
            if(this.errorLoading) {
                this.$Progress.fail();
            }
        }
    },

    methods: {
        lockVault() {
            this.$store.dispatch('lockVault');
        },

        resetIdleTimer() {
            clearTimeout(this.timeOut);

            this.timeOutStarted = (new Date()).getTime();

            this.timeOut = setTimeout(this.lockVault, this.maxIdleTime);
        },

        logout() {
            let logoutForm = new Form({
                user: this.user
            });

            logoutForm.post('/logout')
                .then((data) => {
                    if(data.loggedOut) {
                        window.location.href = '/';
                    }
                })
                .catch((data) => {
                    console.log("Logout from main failed: " + { 'data': data });
                });
        }
    },

    mounted() {
        moment.relativeTimeThreshold('s', 60);
        moment.relativeTimeThreshold('ss', 1);

        this.$store.dispatch('unlockVault');
        this.$store.dispatch('getUser');

        document.onmousemove = this.resetIdleTimer;
        document.onmousedown = this.resetIdleTimer;
        document.ontouchstart = this.resetIdleTimer;
        document.onclick = this.resetIdleTimer;
        document.onscroll = this.resetIdleTimer;
        document.onkeypress = this.resetIdleTimer;

        window.onbeforeunload = () => {
            setTimeout(() => {
                if(this.vaultLockedStatus) {
                    this.logout();
                }
            }, 1000)
        };

        setInterval(() => {
            this.timeRemaining = moment(this.timeOutStarted + this.maxIdleTime).countdown().toString();
        }, 1000)

        setTimeout(() => {
            this.loadDelay = true;
        }, 3000);
    }
});
