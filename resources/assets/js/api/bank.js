import store from '../store';

export default {
    unlockVault(unlockVaultCb, loadCredentialsCb, loadingVaultCb) {
        store.dispatch('startLoading');

        axios.get('/unlock-vault')
            .then(({data}) => {
                store.dispatch('finishLoading');

                if(data) {
                    const master_password = data;

                    unlockVaultCb(master_password);

                    loadingVaultCb(true);

                    store.dispatch('startLoading');

                    axios.get('/credentials')
                        .then(({data}) => {
                            store.dispatch('finishLoading');

                            loadCredentialsCb(data);

                            loadingVaultCb(false);
                        })
                        .catch(response => {
                            store.dispatch('errorLoading');

                            console.log(response);

                            loadingVaultCb(false);
                        });
                } else {
                    store.dispatch('errorLoading');

                    // show error
                }
            })
            .catch(response => {
                store.dispatch('errorLoading');

                console.log(response);
            })
    },

    lockVault(cb) {
        cb();
    },

    loadCredentials(cb) {
        store.dispatch('startLoading');

        axios.get('/credentials')
            .then(({data}) => {
                store.dispatch('finishLoading');

                cb(data);
            })
            .catch(response => {
                store.dispatch('errorLoading');

                console.log(response);
            });
    }
}