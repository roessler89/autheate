import store from '../store';

export default {
    getFormulas(getFormulasCb, loadingFormulasCb) {
        loadingFormulasCb(true);

        store.dispatch('startLoading');

        axios.get('/formulas')
            .then(({data}) => {
                store.dispatch('finishLoading');

                if(data) {
                    getFormulasCb(data);
                }

                loadingFormulasCb(false);
            })
            .catch(response => {
                store.dispatch('errorLoading');

                loadingFormulasCb(false);

                console.log(response)
            });
    },

    loadFormulas(cb) {
        store.dispatch('startLoading');

        axios.get('/formulas')
            .then(({data}) => {
                store.dispatch('finishLoading');

                if(data) {
                    cb(data);
                }
            })
            .catch(response => {
                store.dispatch('errorLoading');

                console.log(response)
            });
    },

    isVowel(character) {
        const vowels = /^[aeiou]$/i;

        return vowels.test(character);
    },

    isConsonant(character) {
        var consonants = /^[bcdfghjklmnpqrstvwxyz]$/i;

        return consonants.test(character);
    },

    isCapital(character) {
        return character == character.toUpperCase();
    },

    offsetCharacter(character, offset) {
        let originalCharCode = character.charCodeAt(0);
        let offsetCharCode = character.charCodeAt(0) + offset;

        if(originalCharCode >= 65 && originalCharCode <= 90) {
            if(offsetCharCode < 65) {
                return String.fromCharCode(91 - (originalCharCode - offsetCharCode));
            } else if(offsetCharCode > 90) {
                return String.fromCharCode(64 + (offsetCharCode - originalCharCode));
            }
        }

        if(originalCharCode >= 97 && originalCharCode <= 122) {
            if(offsetCharCode < 97) {
                return String.fromCharCode(123 - (originalCharCode - offsetCharCode));
            } else if(offsetCharCode > 122) {
                return String.fromCharCode(96 + (offsetCharCode - originalCharCode));
            }
        }

        return String.fromCharCode(offsetCharCode);
    },

    getMajorSystemDigits(characterArray) {
        let majorString = '';

        for(let i = 0; i < characterArray.length; i++) {
            let character = characterArray[i];

            character = character.toLowerCase();

            characterArray[i] = character;
        }

        for(let i = 0; i < characterArray.length; i++) {
            if(characterArray[i] == characterArray[i - 1]) {
                continue;
            }

            if(characterArray[i + 1] == 'h') {
                switch(characterArray[i]) {
                    case 't':
                        majorString = majorString + '1';
                        break;

                    case 'c':
                        majorString = majorString + '6';
                        break;

                    case 's':
                        majorString = majorString + '6';
                        break;

                    case 'p':
                        majorString = majorString + '8';
                        break;

                    default:
                        break;
                }
            } else if(characterArray[i] == 'c' && characterArray[i + 1] == 'k') {
                majorString = majorString + '7';

                i++;
            } else if(characterArray[i] == 'x') {
                majorString = majorString + '70';
            } else {
                switch(characterArray[i]) {
                    case 's':
                        majorString = majorString + '0';
                        break;

                    case 'z':
                        majorString = majorString + '0';
                        break;

                    case 't':
                        majorString = majorString + '1';
                        break;

                    case'd':
                        majorString = majorString + '1';
                        break;

                    case 'n':
                        majorString = majorString + '2';
                        break;

                    case 'm':
                        majorString = majorString + '3';
                        break;

                    case 'r':
                        majorString = majorString + '4';
                        break;

                    case 'l':
                        majorString = majorString + '5';
                        break;

                    case 'j':
                        majorString = majorString + '6';
                        break;

                    case 'c':
                        majorString = majorString + '7';
                        break;

                    case 'k':
                        majorString = majorString + '7';
                        break;

                    case 'g':
                        majorString = majorString + '7';
                        break;

                    case 'q':
                        majorString = majorString + '7';
                        break;

                    case 'v':
                        majorString = majorString + '8';
                        break;

                    case 'f':
                        majorString = majorString + '8';
                        break;

                    case 'p':
                        majorString = majorString + '9';
                        break;

                    case 'b':
                        majorString = majorString + '9';
                        break;

                    default:
                        break;
                }
            }
        }

        return majorString;
    },

    autheatePassword(autheatedPass, vowelOffset, consonantOffset, countVowels, countConsonants, locateCapitals, symbol, useMajorSystem) {
        let characterArray = autheatedPass.split('');
        let passwordArray = [];
        let vowels = 0;
        let consonants = 0;
        let capitalsArray = [];

        /**
         * Go through each character and start the transformation process.
         */
        for (let character of characterArray) {
            /**
             * Check if the character is not a number.
             * If it isn't run the transformations, otherwise simply add it to the passwordArray.
             */

            if(isNaN(parseFloat(character))) {
                /**
                 * Check if the user wants to locate capitals, if the user does and the character is a capital,
                 * push the location (in human terms where characters start from 1, hence +1) of the capital character
                 * to the capitalsArray.
                 */
                if(locateCapitals && this.isCapital(character)) capitalsArray.push(characterArray.indexOf(character) + 1);

                /**
                 * Check if the character is a vowel apply any vowel formulas to the character. If it's not a vowel,
                 * run the consonant formulas.
                 */
                if(this.isVowel(character)) {
                    /**
                     * Check if the user wants a vowel offset applied, if so apply the offset.
                     */
                    if(vowelOffset) character = this.offsetCharacter(character, vowelOffset);

                    /**
                     * Check if the user wants to count the number of vowels in the secret word, if so increase the
                     * vowel count.
                     */
                    if(countVowels) vowels++;
                } else if(this.isConsonant(character)) {
                    /**
                     * Check if the user wants a consonant offset applied, if so apply the offset.
                     */
                    if(consonantOffset) character = this.offsetCharacter(character, consonantOffset);

                    /**
                     * Check if the user wants to count the number of consonants in the secret word, if so increase the
                     * consonant count.
                     */
                    if(countConsonants) consonants++;
                }
            }

            /**
             * Push the character to the password array after any necessary transformations have been applied.
             */
            passwordArray.push(character);
        }

        /**
         * If user wants the Major Mnemonic System applied, run the transformations and assign to the password var.
         *
         * If not, join the passwordArray and assign it to the password var.
         */
        if(useMajorSystem) {
            if(passwordArray.length == 0) {
                var password = this.getMajorSystemDigits(characterArray);
            } else {
                var password = this.getMajorSystemDigits(passwordArray);
            }
        } else {
            var password = passwordArray.join('');
        }

        let capitals = capitalsArray.join('');

        /**
         * Start building the password with any extras the user has enabled.
         */
        if(symbol) password += symbol;

        // If the user wants to count consonants, add the amount to the end of the password string.
        if(countConsonants) password += consonants;

        // If the user wants to count vowels, add the amount to the end of the password string.
        if(countVowels) password += vowels;

        // If the user wants to locate capitals, add the locations to the end of the password string.
        if(locateCapitals) password += capitals;

        // Return transformed password.
        return password;
    }
}