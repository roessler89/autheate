import store from '../store';

export default {
    getUsers(cb) {
        store.dispatch('startLoading');

        axios.get('/admin/users')
            .then(({data}) => {
                store.dispatch('finishLoading');

                if(data) {
                    cb(data);
                } else {
                    store.dispatch('errorLoading');

                    // show error
                }
            })
            .catch(response => {
                store.dispatch('errorLoading');

                console.log(response);
            })
    },

    getUser(cb) {
        store.dispatch('startLoading');

        axios.get('/users/current')
            .then(({data}) => {
                store.dispatch('finishLoading');

                if(data) {
                    cb(data);
                } else {
                    store.dispatch('errorLoading');

                    // show error
                }
            })
            .catch(response => {
                store.dispatch('errorLoading');

                console.log(response);
            })
    },

    verifyUser(password) {
        store.dispatch('startLoading');

        axios.post('/users/verify', password)
    }
}