
window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

window.$ = window.jQuery = require('jquery');

require('bootstrap-sass');

/**
 * Vue is a modern JavaScript library for building interactive web interfaces
 * using reactive data binding and reusable components. Vue's API is clean
 * and simple, leaving you to focus on building your next great project.
 */

window.Vue = require('vue');

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common = {
    'X-CSRF-TOKEN': window.Laravel.csrfToken,
    'X-Requested-With': 'XMLHttpRequest'
};

/**
 * Stanford Javascript Crypto Library (SJCL) is a library that allows us to encrypt and decrypt information client-side
 * using Javascript. To learn more about SJCL, please see:
 * https://github.com/bitwiseshiftleft/sjcl
 */

window.sjcl = require('sjcl');

/**
 * zxcvbn is used for password strength estimation, developed by Dropbox password crackers. To learn more, please see:
 * https://github.com/dropbox/zxcvbn
 */

window.zxcvbn = require('zxcvbn');

/**
 * momentjs is to parse, validate, manipulate, and display dates and times in JavaScript. To learn more, please see:
 * https://momentjs.com/
 */

window.moment = require('moment');

/**
 * moment-countdown is used to simplify the process of creating countdown timers that work with momentjs. To learn more, please see:
 * https://github.com/icambron/moment-countdown
 */

window.momentCountdown = require('moment-countdown');

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from "laravel-echo"

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: 'your-pusher-key'
// });
