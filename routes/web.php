<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/about', 'PagesController@about')->name('about');

Route::get('/about-formulas', 'PagesController@aboutFormulas')->name('about-formulas');

Auth::routes();

Route::get('/users/current', 'UserController@current');

Route::get('/users/account', 'UserController@account');

Route::post('/users/verify', 'UserController@verify');

Route::post('/users/recaptcha', 'UserController@recaptcha');

Route::get('/admin', 'AdminController@index');

Route::get('/admin/credentials', 'AdminController@credentials');

Route::get('/admin/users', 'AdminController@users');

Route::get('/admin/users/{user}', 'AdminController@showUser');

Route::patch('/admin/users/{user}', 'AdminController@updateUser');

Route::resource('users', 'UserController', ['only' => ['update']]);

Route::resource('credentials', 'CredentialController');

Route::resource('formulas', 'FormulaController');

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/unlock-vault', 'VaultController');
